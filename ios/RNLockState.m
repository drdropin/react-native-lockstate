#import <notify.h>

#import "RNLockState.h"

static NSString *const lockStateDidChange = @"lockStateDidChange";
static NSString *const lockComplete = @"lockComplete";
static NSString *const lockState = @"lockState";

@implementation RNLockState
{
    int notify_token_lockstate;
    int notify_token_lockcomplete;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE()

+ (id)allocWithZone:(NSZone *)zone {
    static RNLockState *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [super allocWithZone:zone];
    });
    return sharedInstance;
}

- (NSArray<NSString *> *)supportedEvents
{
    return @[lockStateDidChange];
}

- (void)startObserving
{
    notify_register_dispatch("com.apple.springboard.lockstate", &notify_token_lockstate, dispatch_get_main_queue(), ^(int token) {
        uint64_t state = UINT64_MAX;
        notify_get_state(token, &state);
        [self handleLockStateChange:state];
    });
}

- (void)stopObserving
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleLockStateChange:(uint64_t)state
{
    if (state == 0) {
        [self sendEventWithName:lockStateDidChange body:@{lockState: @false}];
    } else {
        [self sendEventWithName:lockStateDidChange body:@{lockState: @true}];
    }
}
@end
  
